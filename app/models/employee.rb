class Employee < ApplicationRecord
	mount_uploader :profile_image, ProfileImageUploader
	has_many		:ref_documents
	belongs_to 	:country
	belongs_to	:user
  accepts_nested_attributes_for :ref_documents

  validates :user_id, presence: true,
            uniqueness: {case_sensitive: false}
	validates :father_name, presence: true
  VALID_PHONE_REGEX = /^(0[1-9]|1[012])\-(19|20)\d{2}$/
  validates :contact_number, :presence => {:message => 'hello world, bad operation!'},
                       :numericality => true,
                       :length => { :minimum => 7, :maximum => 15 }
  validates :nrc, presence: true,
            uniqueness: { case_sensitive: false }
  validates :employee_no, presence: true,
  					uniqueness: { case_sensitive: false }
  validates :gender, presence: true
	validates	:nationality, presence: true
	validates :dob, presence: true
	validates :address, presence: true
end
