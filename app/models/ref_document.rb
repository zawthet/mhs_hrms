class RefDocument < ApplicationRecord
	mount_uploader :file_name, FileNameUploader
	belongs_to 	:employee
end
