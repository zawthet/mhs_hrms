class LeaveApply < ApplicationRecord
	attr_accessor :leave_apply_id, :name, :attachment
	mount_uploader :attachment, AttachmentUploader
	belongs_to	:user, required: false
	belongs_to	:leave_type, required: false

	# def self.search(search_employee_id,  search_start_date, search_end_date)
		
	# where(['employee_id LIKE ? AND start_date <= ? AND end_date >= ?', "%#{search_employee_id}%", "%#{search_start_date}%","%#{search_end_date}"])
	
	# end
end
