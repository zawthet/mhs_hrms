class EmployeesController < ApplicationController
  before_action :logged_in_user, only: [:index, :new, :create, :update, :destroy]
  before_action :set_employee, only: [:show, :edit, :update, :destroy]
  skip_before_action :verify_authenticity_token

  def index
    @Home = 'Employee Home'
    @Link = 'Employee List'
    @roles = Role.all
    @employees = Employee.joins(:user)
      .select("employees.*,users.fullname,email,role_id").order(id: :desc)
      .paginate(page: params[:page], per_page: 10)
  end

  # GET /employees/1
  # GET /employees/1.json
  def show
    @Home = 'Employee Home'
    @Link = 'Employee Details'
    @employee_attachments = @employee.ref_documents.all
    @employee = Employee.joins(:user).select('employees.*, users.email').find(params[:id])
    @readonly = false
    if params[:show]
      @readonly = true
    end
    @male,@female = false
    if @employee.gender == 0
      @female = true
    elsif @employee.gender == 1
      @male = true
    end
  end

  # GET /employees/new
  def new
    @Home = 'Employee Home'
    @Link = 'Registration'
    @employee = Employee.new
    @employee_attachment = @employee.ref_documents.build
    @roles = Role.all
  end

  # GET /employees/1/edit
  def edit
    @Home = 'Employee Home'
    @Link = 'Employee Update'
    @employee_attachments = @employee.ref_documents.all
    @employee = Employee.joins(:user).select('employees.*, users.email').find(params[:id])
    @readonly = false
    if params[:show]
      @readonly = true
    end
    @male,@female = false
    if @employee.gender == 0
      @female = true
    elsif @employee.gender == 1
      @male = true
    end
  end

  # POST /employees
  # POST /employees.json
  def create

    @employee = Employee.new(employee_params)
    @employee[:country_id] = params[:employee][:country_id]
    @employee[:user_id] = params[:employee][:user_id]

    respond_to do |format|
      if @employee.save
        params[:ref_documents]['file_name'].each do |a|
          @employee_attachment = @employee.ref_documents.create!(:file_name => a)
        end
        @user = User.find(params[:employee][:user_id])
        @user.update(email: params[:employee][:email], password: params[:employee][:password])
        format.html { redirect_to employee_path(id:@employee.id, show:@employee.id) }
        format.json { render :show, status: :ok, location: @employee }
      else 
        format.html { render :edit }
        format.json { render json: @employee.errors, status: :unprocessable_entity }
      end
    end 

  end

  # PATCH/PUT /employees/1
  # PATCH/PUT /employees/1.json
  def update
    @employee[:country_id] = params[:employee][:country_id]
    @employee[:user_id] = params[:employee][:user_id]
    
    respond_to do |format|
      if @employee.update(employee_params)

        params[:ref_documents]['file_name'].each do |a|
          @employee_attachment = @employee.ref_documents.update(:file_name => a)
        end

        @user = User.find(params[:employee][:user_id])
        @user.update(email: params[:employee][:email], password: params[:employee][:password])

        format.html { redirect_to edit_employee_path(id:@employee.id, show:@employee.id) }
        format.json { render :show, status: :ok, location: @employee }        

      else
        format.html { render :edit }
        format.json { render json: @employee.errors, status: :unprocessable_entity }
      end
    end
 
  end

  # DELETE /employees/1
  # DELETE /employees/1.json
  def destroy
  	respond_to do |format|
      if @employee.destroy
        format.html { redirect_to @employee }
        format.json { render :show, status: :ok, location: @employee }
      else
        format.html { render :edit }
        format.json { render json: @employee.errors, status: :unprocessable_entity }
      end
    end
  end

  def deleteDocument
    @ref_doc = RefDocument.find(params[:document_id])
    @ref_doc.destroy
    render :json => @ref_doc.to_json
  end

  def download_pdf
    ref_doc = RefDocument.find(params[:document_id])
    send_file(
      "#{Rails.root}/public#{ref_doc.file_name}",
      filename: "#{ref_doc.file_name}.pdf",
      type: "application/pdf"
    )
    render :json => ref_doc.to_json
  end

	private

    def set_employee
      @employee = Employee.find(params[:id])
    end

    def employee_params
      params.require(:employee).permit( :employee_no, :user_id, :father_name, :nationality,
       :gender, :nrc, :contact_number, :age, :dob, :address, :profile_image,
       :martial_status, :country_id, :fullname, ref_documents_attributes: [:id, :file_name] )
    end

end
