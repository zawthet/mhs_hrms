class ApplicationController < ActionController::Base
  protect_from_forgery with: :exception, prepend: true
  # protect_from_forgery with: :exception
  include SessionHelper
    rescue_from CanCan::AccessDenied do |exception|
      flash[:notice] = 'Access Denied'
      redirect_to root_path
    end

end
