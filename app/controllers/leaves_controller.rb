class LeavesController < ApplicationController
    before_action :logged_in_user, only: [:index, :new, :create, :update, :destroy]
   
    def index
        @Home = 'Leave Management'
        @Link = 'My Leave History'
        @leaves = LeaveApply.joins(:leave_type, :user).select("leave_applies.*, leave_types.*,  users.*").order(id: :desc).paginate(page: params[:page],per_page:5 )
    end

    def leave_report
        @Home = 'Leave Management'
        @Link = 'Leave Report'
            @leave_balance = []
            @users = User.all.joins(:employees, :leave_applies).select("users.*, employees.employee_no, leave_applies.*")
        
                @users.each do |user|
                @days = LeaveApply.where("user_id = ?", user.id).sum(:numberofday)
                @total_days = (10-@days)
                a = {'employee_no' => user.employee_no, 'fullname' => user.fullname, 'Available_annual_leave' => 10, 'leave_balance' => @total_days}
                @leave_balance.push(a)
                end
         puts @leave_balance.inspect
        # @sum = 0
        #  @days.each do |i.numberofday|
        #     puts i
        #     #  @sum += i
        # end
       
        # @search = LeaveReportSearch.new(params[:search])
        # @leavesreport = @search.scope
        # @leavereports = LeaveApply.search(params[:employee_id], params[:start_date], params[:end_date]).order(id: :desc).paginate(page: params[:page],per_page:5 )
        # @leavereports = LeaveApply.all
    end

    def new
        @Home = 'Leave Management'
        @Link = 'Apply Leave'
        @leave = LeaveApply.joins(:leavetype, :user).select("leave_types.*, leave_applies.*, users.name")
    end

    def create
        @leave = LeaveApply.new(leaves_params)
        @leave[:user_id] = params[:leave_apply][:user_id]
        @leave[:leave_type_id] = params[:leave_apply][:leave_type_id]
        respond_to do |format|
            if @leave.save
                format.html { redirect_to leaves_path}
                format.json { render :show, status: :ok, location: @leave}
            else
                format.html { render :new }
                format.json { render json: @leave.errors, status: :unprocessable_entity}
            end
        end
        
    end

    def destroy

    end

    private

    def redirect_cancel
        redirect_to leave_path if params[:cancel]
    end

    def set_employee
        @leave = LeaveApply.find(params[:id])
      end

    def leaves_params
        params.require(:leave_apply).permit( :employee_id, :leave_type_id, :start_date, :end_date, :numberofday, :reason, :relieving, :search, :attachment)
    end
end
