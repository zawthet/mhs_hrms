class CheckinoutsController < ApplicationController
  before_action :logged_in_user, only: [:index, :edit, :update, :destroy]
  load_and_authorize_resource

  def index
    @Home = 'Late Management'
    @Link = 'Late Arrival List'
    beginning_of_month = Date.today.beginning_of_month
    end_of_month = beginning_of_month.end_of_month
    @attendances = Checkinout.all.joins(:user)
      .select("checkinouts.*,users.fullname").order(id: :desc)
      .where(date: beginning_of_month..end_of_month)
      .paginate(page: params[:page], per_page: 10)
  end

  def late_report
    @Home = 'Late Management'
    @Link = 'Late Arrival Report'
    @beginning_of_month = Date.today.beginning_of_month
    @end_of_month = @beginning_of_month.end_of_month
    @salary = 200000
    @users = User.all.joins(:employees).select("users.*, employees.employee_no")
  end


end
