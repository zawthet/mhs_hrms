Rails.application.routes.draw do

  resources :users
  resources :employees
  resources :checkinouts
  resources :ref_documents
  resources :leaves, only: [:new, :index]
  root 'employees#index'
  
  get    '/login',   	          to: 'session#new'
  post   '/login',   	          to: 'session#create'
  get 	 '/logout',  	          to: 'session#destroy'
  get 	 '/employees/new',	    to: 'employees#new'
  get 	 '/employees',	        to: 'employees#index'
  get    '/attendances',        to: 'checkinouts#index'
  get    '/lates',              to: 'checkinouts#late_report'
  post   '/leaves/new',         to: 'leaves#create'
  get    '/leave_report',       to: 'leaves#leave_report'
  post   '/deleteDoc',          to: 'employees#deleteDocument'
  post   '/download_pdf',       to: 'employees#download_pdf'
 	
 end
