class CreateEmployees < ActiveRecord::Migration[5.2]
  def change
    create_table :employees do |t|
    	t.string 	:name , null: false, default: ""
    	t.string	:father_name
      t.string	:nationality
      t.integer :gender
      t.string	:nrc
      t.date		:dob
      t.integer :age
      t.integer	:contact_number
      t.text		:address
      t.string	:employee_no
      t.boolean	:isdeletec
      t.string	:profile_image
      t.integer :martial_status
      t.string 	:created_by
      t.string 	:updated_by
      t.references :country
      t.references  :user

      t.timestamps
    end
  end
end
