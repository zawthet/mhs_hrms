class CreateLeaveApplies < ActiveRecord::Migration[5.2]
  def change
    create_table :leave_applies do |t|
    	t.date		:start_date
    	t.date 		:end_date
    	t.integer	:numberofday
    	t.string	:reason
    	t.string 	:created_by
			t.string 	:updated_by
			t.string  :relieving
			t.string  :attachment
    	t.references	:user
    	t.references	:leave_type

      t.timestamps
    end
  end
end
