class CreateRefDocuments < ActiveRecord::Migration[5.2]
  def change
    create_table :ref_documents do |t|
    	t.string	:file_name
    	t.string 	:created_by
      t.string 	:updated_by
      t.references :employee

      t.timestamps
    end
  end
end
