class CreateCountries < ActiveRecord::Migration[5.2]
  def change
    create_table :countries do |t|
    	t.string	:name
    	t.string	:create_by
    	t.string	:update_by

      t.timestamps
    end
  end
end
