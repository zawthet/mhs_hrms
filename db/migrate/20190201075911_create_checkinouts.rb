class CreateCheckinouts < ActiveRecord::Migration[5.2]
  def change
    create_table :checkinouts do |t|
    	t.date	:date
    	t.time 	:timeIn
    	t.time 	:timeOut
    	t.time 	:workHour
    	t.string 	:created_by
      t.string 	:updated_by
      t.references  :user

      t.timestamps
    end
  end
end
