class CreateLeaveTypes < ActiveRecord::Migration[5.2]
  def change
    create_table :leave_types do |t|
    	t.string	:name
    	t.string	:status
    	t.string 	:created_by
      t.string 	:updated_by

      t.timestamps
    end
  end
end
